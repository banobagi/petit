
$(document).ready(function(){
	$(window).on("load resize", function () {
		var winH = $(window).height();
		$('.main-visual').height(winH);
		if ($('body').attr('data-mobile') == 'false'){

		}else{

		}
	});
	function gnbMotion(){
		var winH = $(window).height();
		if ($(window).scrollTop()<winH) {
			var isBody = $('body').attr('class');
			if(isBody != "no-scroll"){
				$('.header').stop().addClass('type-main');
			}
		}
		else {
			$('.header').stop().removeClass('type-main');
		}
	}
	gnbMotion();
	$(window).scroll(function() {
		gnbMotion();
	});
	$('.js-main-slider').cycle({
		swipe:true,
		fx:'scrollHorz',
	    slides:'.main-visual__item',
		prev:'.js-main-prev',
		next:'.js-main-next',
		caption:'.main-page__group',
		captionTemplate:  '<span class="main-page__now">0{{slideNum}}</span><span class="main-page__total">/ 0{{slideCount}}</span>'
	});
	//화살표 블랙만들기
	$(window).on("load", function () {
		visualBtn();
	});
	function visualBtn(){
		var isState = $('.cycle-slide-active').attr('class');
		var findBlack = "type-black"
		if(isState.indexOf(findBlack) != -1){
			$('.main-visual__btn').addClass(findBlack);
		}else{
			$('.main-visual__btn').removeClass(findBlack);
		}
	}
	$( '.js-main-slider' ).on( 'cycle-after', function( event, opts ) {
		visualBtn();
	});
	$('.js-focus-slider').cycle({
		swipe:true,
		fx:'scrollHorz',
		timeout:0,
	    slides:'.focus__item',
		prev:'.js-focus-prev',
		next:'.js-focus-next'
	});
	//퀵 항상보이기
	$('.quick').addClass('is-active');
	//포커스 상품추가하였습니다 부분
	$('.js-alarm').off('click').on('click',function(e){
		e.preventDefault();
		var alarm = $(this).closest('.focus__item').find('.focus__alarm');
		alarm.stop().addClass('is-active')
		var alarmTime = setTimeout(function(){alarm.removeClass('is-active')},1000);
	});


	/* setCookie function */
	function setCookie(cname, value, expire) {
	   var todayValue = new Date();
	   // 오늘 날짜를 변수에 저장

	   todayValue.setDate(todayValue.getDate() + expire);
	   document.cookie = cname + "=" + encodeURI(value) + "; expires=" + todayValue.toGMTString() + "; path=/;";
	}
	// Get cookie function
	function getCookie(name) {
	   var cookieName = name + "=";
	   var x = 0;
	   while ( x <= document.cookie.length ) {
	      var y = (x+cookieName.length);
	      if ( document.cookie.substring( x, y ) == cookieName) {
	         if ((lastChrCookie=document.cookie.indexOf(";", y)) == -1)
	            lastChrCookie = document.cookie.length;
	         return decodeURI(document.cookie.substring(y, lastChrCookie));
	      }
	      x = document.cookie.indexOf(" ", x ) + 1;
	      if ( x == 0 )
	         break;
	      }
	   return "";
	}




	// 하루동안 안열기 쿠키 저장
	$(function() {
	   var closeTodayBtn = $('.js-popup-m-today');
	   var closeTodayBtn2 = $('.js-popup-m-today2');
	   var closeTodayBtn3 = $('.js-popup-m-today3');

	   // 버튼의 클래스명은 closeTodayBtn

	   closeTodayBtn.click(function(e) {
	      setCookie( "popup20180910", "end" , 1);
	      // 하루동안이므로 1을 설정
		  e.preventDefault();
		  $('.popup-m.type-first').removeClass('is-active')
	      // 현재 열려있는 팝업은 닫으면서 쿠키값을 저장
	   });

	   closeTodayBtn2.click(function(e) {
	      setCookie( "popup20180912", "end" , 1);
	      // 하루동안이므로 1을 설정
		  e.preventDefault();
		  $('.popup-m.type-second').removeClass('is-active')
	      // 현재 열려있는 팝업은 닫으면서 쿠키값을 저장
	   });

	   closeTodayBtn3.click(function(e) {
	      setCookie( "popup20181226", "end" , 1);
	      // 하루동안이므로 1을 설정
		  e.preventDefault();
		  $('.popup-m.type-third').removeClass('is-active')
	      // 현재 열려있는 팝업은 닫으면서 쿠키값을 저장
	   });

	});


	//쿠키체크후 팝업열기
	var result = getCookie('popup20180910');
	var result2 = getCookie('popup20180912');
	var result3 = getCookie('popup20181226');
	if (result != 'end') {
		$('.popup-m.type-first').addClass('is-active')
	}
	if (result2 != 'end') {
		$('.popup-m.type-second').addClass('is-active')
	}
	if (result3 != 'end') {
		$('.popup-m.type-third').addClass('is-active')
	}
	$('.js-popup-m-close').on('click',function(e){
		e.preventDefault();
		$(this).closest('.popup-m').removeClass('is-active')
	});
});

//스크롤 모션
$(function(){
	$(".js-motion").each(function(){
		var $this = $(this);
		var n = function() {
			if ($this.offset().top < $(window).scrollTop() + $(window).height() && $this.offset().top > $(window).scrollTop() - $this.height()) {
				$this.addClass('on');
			}
		};
		var b = function() {
			if ($this.offset().top < $(window).scrollTop() + $(window).height() && $this.offset().top > $(window).scrollTop() - $this.height()) {
				$this.addClass('on');
			}else{
				$this.removeClass('on');
			}
		};
		$(window).on("scroll", n)
		$(window).on("load", b)
	});
});
dayGap ();
var ddaytimer = setInterval (dayGap, 1000)
//디데이 계산기
function dayGap () {
	var dday = new Date("September 24, 2018 00:00:00");//디데이
	var ddayChange = dday.getTime();
	var nowday = new Date();//현재
	nowday = nowday.getTime();//밀리세컨드 단위변환
	var distance = ddayChange - nowday;//디데이에서 현재까지 뺀다.

	var d = Math.floor(distance / (1000 * 60 * 60 * 24));//일

	var h = Math.floor((distance / (1000*60*60)) % 24);//시간
	var m = Math.floor((distance / (1000*60)) % 60);//분
	var s = Math.floor((distance / 1000) % 60);//초

	var leadingZeros = function (date, num) {
		var zero = '';
		date = date.toString();

		if (date.length < num) {
		for (i = 0; i < num - date.length; i++)
			zero += '0';
		}
		return zero + date;
	}
	document.getElementById("time-day").innerHTML = leadingZeros(d, 2);
	document.getElementById("time-hour").innerHTML = leadingZeros(h, 2);
	document.getElementById("time-minute").innerHTML = leadingZeros(m, 2);
	document.getElementById("time-second").innerHTML = leadingZeros(s, 2);
}
