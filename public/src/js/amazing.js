// Call & init
$(document).ready(function(){
	function amazingWidth(){
		var cur = $('.amazing__wrap');
		var width = cur.width()+'px';
		cur.find('.amazing__resize img').css('width', width);
		cur.find('.blind').css('width', width/2);
	}
	var amazingSlider = $('.js-slider').bxSlider({
		mode: 'fade',
		moveSlides: 1,
		slideMargin: 0,
		// infiniteLoop: true,
		minSlides: 1,
		maxSlides: 1,
		speed: 800,
		touchEnabled:false,
		oneToOneTouch:false,
		pager: false,
		preventDefaultSwipeX:false,
		nextSelector:'.js-slider-next',
		onSliderLoad:function(){
			$(window).on('load resize',function(){
				amazingWidth();
			});
		}
	});
	amazingWidth();
	$('.js-amazing').each(function(){
		var cur = $(this);
		drags(cur.find('.amazing__handle'), cur.find('.amazing__resize'), cur);
	});
	$('.js-slider-prev').on('click', function (e) {
		e.preventDefault();
		amazingSlider.goToPrevSlide();
		return false;
	});
	$('.js-slider-next').on('click', function (e) {
		alert(1)
		e.preventDefault();
		amazingSlider.goToNextSlide();
		return false;
	});
});

// Update sliders on resize.
// Because we all do this: i.imgur.com/YkbaV.gif


function drags(dragElement, resizeElement, container) {

	// Initialize the dragging event on mousedown.
	dragElement.on('mousedown touchstart', function(e) {

		dragElement.addClass('drag');
		resizeElement.addClass('resizable');

		// Check if it's a mouse or touch event and pass along the correct value
		var startX = (e.pageX) ? e.pageX : e.originalEvent.touches[0].pageX;

		// Get the initial position
		var dragWidth = dragElement.outerWidth(),
	    posX = dragElement.offset().left + dragWidth - startX,
	    containerOffset = container.offset().left,
	    containerWidth = container.outerWidth();

		// Set limits
		minLeft = containerOffset + 10;
		maxLeft = containerOffset + containerWidth - dragWidth - 10;

		// Calculate the dragging distance on mousemove.
		dragElement.parents().on("mousemove touchmove", function(e) {

		// Check if it's a mouse or touch event and pass along the correct value
		var moveX = (e.pageX) ? e.pageX : e.originalEvent.touches[0].pageX;

		leftValue = moveX + posX - dragWidth;

	// Prevent going off limits
	if ( leftValue < minLeft) {
		leftValue = minLeft;
	} else if (leftValue > maxLeft) {
		leftValue = maxLeft;
	}

	// Translate the handle's left value to masked divs width.
	widthValue = (leftValue + dragWidth/2 - containerOffset)*100/containerWidth+'%';

	// Set the new values for the slider and the handle.
	// Bind mouseup events to stop dragging.
	$('.drag').css('left', widthValue).on('mouseup touchend touchcancel', function () {
		$(this).removeClass('drag');
		resizeElement.removeClass('resizable');
	});
		$('.resizable').css('width', widthValue);
	}).on('mouseup touchend touchcancel', function(){
		dragElement.removeClass('drag');
		resizeElement.removeClass('resizable');
	});
	e.preventDefault();
	}).on('mouseup touchend touchcancel', function(e){
		dragElement.removeClass('drag');
		resizeElement.removeClass('resizable');
	});
}
