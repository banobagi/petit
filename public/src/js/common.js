$(document).ready(function(){

	$(window).on("load resize", function () {
		$('body').attr('data-mobile',
			(function(){
				var r = ($(window).width() <= 1024) ? true : false;
				return r;
			}())
		);

		$('body').attr('only-mobile',
			(function(){
				var r = ($(window).width() <= 680) ? true : false;
				return r;
			}())
		);
		headerH = $('.header').height();
		var footerH = $('.footer').outerHeight();
		$('body').css('padding-bottom',footerH);
		if ($('body').attr('data-mobile') == 'false'){
			//pc로 전환시 모바일 메뉴 다 닫기
			$('.mobile-menu, .gnb').removeClass('is-active');
			$('.quick__item').on('mouseenter',function(){
				$(this).addClass('is-active');
			}).on('mouseleave',function(){
				$(this).removeClass('is-active')
			})
			var isBody = $('body').attr('class');
			if(isBody == "no-scroll"){
				scrollStart();
			}
			//마우스오버 효과
			$('.js-hover').on('mouseenter',function(){
				$(this).addClass('is-active');
			}).on('mouseleave',function(){
				var isFocus = $(this).attr('class')
				var isFocus2 = isFocus.split(' ')
				if(isFocus2[0] == "focus__item"){$('.focus__alarm').removeClass("is-active");}
				$(this).removeClass('is-active');
			});
			//셀렉트 디자인 입히기
			$('.select__list').mCustomScrollbar({
				theme:'dark',
				advanced:{
					updateOnContentResize: true
				}
			});
		}else{
			//마우스오버효과 끄기
			$('.js-hover').off('mouseenter mouseleave')
			//셀렉트 디자인끄지
			$('.select__list').mCustomScrollbar("destroy");
		}

		scroll_button();
	   function scroll_button(){
		   var $button = $('.scroll-top');

		   if(!$button.length) { return; }
		   var scrollBtnW = $button.width();
		   // circle progress scroll
		   $.circleProgress.defaults.animation = false;
		   $.circleProgress.defaults.value = 0;
		   $.circleProgress.defaults.size = scrollBtnW;
		   $.circleProgress.defaults.startAngle = -Math.PI / 4 * 2;
		   $.circleProgress.defaults.thickness = '2';
		   $.circleProgress.defaults.emptyFill = 'rgba(238, 238, 238, 1)';
		   $.circleProgress.defaults.fill = { color: '#ff5e73' };


		   $('.scroll-top__progress').circleProgress();

		   $(window).on('load resize',function(){
			   scroll_button_resize();
		   });
		   $('.js-scroll-top').on('click', function(e) {
			   e.preventDefault();
			   $('html, body').stop().animate({
				   scrollTop: 0
			   }, 500);
		   });
	   }

	   function scroll_button_resize(){
		   $('.scroll-top__progress').circleProgress('redraw');
	   }

		//progressBar
	   $.progressIndicator({
		   direction : 'top',
		   barColor: 'rgb(255, 94, 115)',
		   percentageEnabled : false,
		   percentageColor: '#ff5e73',
		   easingSpeed : 0.5,
		   height: 3,
		   target : 'body', // selector
		   onStart : function(){
		   },
		   onEnd : function(){
		   },
		   onProgress : function(perecent){
		   }
	   });

	});
	//pc gnb 오버효과
	$('.gnb__depth1-list').on('mouseenter', function(){
		if ($('body').attr('data-mobile') == 'false'){
			$('.gnb, .header').addClass('is-active');
		}
	})
	$('.header').on('mouseleave', function(){
		if ($('body').attr('data-mobile') == 'false'){
			$('.gnb, .header').removeClass('is-active');
		}
	});
	//모바일 뎁스메뉴오픈효과
	$('.gnb__depth1-link').on('click',function(e){
		var gnbState = $(this).closest('.gnb__depth1-item');
		var gnbStateClass1 = gnbState.attr('class')
		var gnbStateClass2 = gnbStateClass1.split(' ');
		if(gnbStateClass2[1] != 'type-1depth'){
			e.preventDefault();
			if ($('body').attr('data-mobile') == 'true'){
				if(gnbState.hasClass('is-active')){
					gnbState.removeClass('is-active');
				}else{
					$('.gnb__depth1-item').removeClass('is-active');
					gnbState.addClass('is-active');
				}
			};
		}
	});
	//모바일메뉴 오픈
	$('.js-open-m').on('click',function(e){
		var gnbState = $('.mobile-menu');
		e.preventDefault();
		if(gnbState.hasClass('is-active')){
			$('.mobile-menu, .gnb').removeClass('is-active');
			$('#progress-indicator').removeClass('is-hide');
			scrollStart()
		}else{
			$('.mobile-menu, .gnb').addClass('is-active');
			$('#progress-indicator').addClass('is-hide');
			scrollStop()
		}
	});
	$('.js-click-m').on('click',function(e){
		if ($('body').attr('data-mobile') == 'false'){
			// e.preventDefault();
		}else{
			$(this).toggleClass('is-active');
		}
	});

	//기본 탭 링크
	$('.js-tab-link').on('click',function(e){
		e.preventDefault();
		var tab_id = $(this).parent().attr('data-tab');
		$(this).parent().addClass('is-active').siblings().removeClass('is-active');
		$("."+tab_id).addClass('is-active').siblings().removeClass('is-active');
	});


	var bodyY;
	function scrollStop(){
		bodyY = $(window).scrollTop();
		$('html, body').addClass("no-scroll");
		$('.common').css("top",-bodyY);
	}
	function scrollStart(){
		$('html, body').removeClass("no-scroll");
		$('.common').css('top','auto')
		bodyY = $('html,body').scrollTop(bodyY);

	}

	//로그인 바노바기
	$('.js-bano-close').on('click',function(e){
		e.preventDefault();
		$(this).parents('.p-login__layer').removeClass('is-active');
	})
	$('.js-open-layer').on('click',function(e){
		e.preventDefault();
		$('.p-login__layer').addClass('is-active');
	})
	//로그인 open/close
	$('.js-login-close').on('click',function(e){
		e.preventDefault();
		$('.p-login, .p-login-dim').fadeOut(500);
		scrollStart();
	});
	$('.js-login-open').on('click',function(e){
		e.preventDefault();
		$('.p-login, .p-login-dim').fadeIn(500);
		scrollStop();
	});

	//오시는길 문자
	$('.js-sms-close').on('click',function(e){
		e.preventDefault();
		$('.popup-sms, .dim.type-sms').fadeOut();
		scrollStart();
	});
	$('.js-sms-open').on('click',function(e){
		e.preventDefault();
		$('.popup-sms, .dim.type-sms').fadeIn();
		scrollStop();
	});
	//퀵 항상보이기
	$('.quick').addClass('is-active');

	$('.js-tab-link').on('click',function(e){
		e.preventDefault();
		var tab_id = $(this).parent().attr('data-tab');
		$(this).parent().addClass('is-active').siblings().removeClass('is-active');
		$("."+tab_id).addClass('is-active').siblings().removeClass('is-active');
	});

	$('.js-popup-open').on('click',function(e){
		e.preventDefault();
		$('.popup-t').addClass('is-active');
		scrollStop();
	});

	$('.js-popup-close').on('click',function(e){
		e.preventDefault();
		$('.popup-t').removeClass('is-active');
		scrollStart();
	});

	//셀렉트박스 열기/닫기
	$(".select__tit").on('click',function(e){
		e.preventDefault();
		var select = $('.select').attr('class');
		var selectIs = select.split(' ');
		if(selectIs[1] == 'is-active'){
			$(this).parent().removeClass('is-active');
			$(this).parent().find('.select2__subject, .select__list').removeClass('is-active');
			$(this).parents('.sub-contact__item').removeClass('is-active');
			if ($('body').attr('data-mobile') == 'true'){
				scrollStart();
			}
		}else{
			$(this).parent().addClass('is-active')
			$(this).parent().find('.select2__subject, .select__list').addClass('is-active');
			$(this).parents('.sub-contact__item').addClass('is-active');
			if ($('body').attr('data-mobile') == 'true'){
				scrollStop();
			}
		}
	})

	//셀렉트박스 선택
	$(".select__item").on('click',function(e){
		e.preventDefault();
		var isTxt = $(this).text();
		$('.select__item').removeClass('is-on');
		$(this).addClass('is-on');
		$(this).parents('.select').removeClass('is-active')
		$(this).parents('.sub-contact__item').removeClass('is-active');
		$(this).parents('.select').find('.select2__subject, .select__list').removeClass('is-active');
		$('.select__tit').text(isTxt);
		if ($('body').attr('data-mobile') == 'true'){
			scrollStart();
		}

	});

	$('.js-content-link').on('click',function(e){
		e.preventDefault();
		var dataName = $(this).attr('data-tab');
		var dataSet = $('.'+dataName).offset().top;
		$("html, body").animate({
			scrollTop:dataSet
		},500);
	});
	//모바일 퀵 상단지나서 나오게하기
	function moQuickhide(){
		if ($('body').attr('data-mobile') == 'true'){
			var winHalf = $(window).height()/2;
			if ($(window).scrollTop()<winHalf) {
				$('.quick.type-mobile').stop().removeClass('is-show');
			}
			else {
				$('.quick.type-mobile').stop().addClass('is-show');
			}
		}
	}
	moQuickhide();
	$(window).scroll(function() {
		moQuickhide();
	});
	//이벤트 탭 버튼
	$('.js-tab-btn').on('click', function(){
		//var p = $(this).parents('[data-product]');
		var state = $('.tab-v3__list');
		if(state.hasClass('is-active')){
			$(this).removeClass('is-active')
			state.removeClass('is-active')
		} else {
			$(this).addClass('is-active')
			state.addClass('is-active')
		}
	});
	//이벤트 탭
	$('.tab-v3__link').on('click',function(e){
		var tabText = $(this).text();
		e.preventDefault();
		$(this).closest('.tab-v3__item').addClass('is-active').siblings().removeClass('is-active');
		$('.tab-v3__list, .tab-v3__btn').removeClass('is-active');
		$('.tab-v3__btn').text(tabText)
	}).on('mouseenter',function(){
		$(this).closest('.tab-v3__item').addClass('is-hover');
	}).on('mouseleave',function(){
		$(this).closest('.tab-v3__item').removeClass('is-hover');
	})
});
