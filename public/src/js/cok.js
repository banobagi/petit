$(document).ready(function(){
	var cnt = 1;
	$('.js-cok-btn').on('click',function(e){
		e.preventDefault();
		var isActive = $(this).closest('.cok__item').attr('class');
		var isActive2 = isActive.split(' ');
		if(isActive2[1] == 'is-active'){
			$(this).closest('.cok__item').removeClass('is-active').find('.cok__body').slideUp(500);
			$(this).closest('.cok__item').find('.js-cok-txt').text('자세히보기')
		}else{
			$(this).closest('.cok__item').siblings().removeClass('is-active').find('.cok__body').slideUp(500);
			$(this).closest('.cok__item').addClass('is-active').find('.cok__body').slideDown(500);
			$(this).closest('.cok__item').find('.js-cok-txt').text('닫기')
			$(this).closest('.cok__item').siblings().find('.js-cok-txt').text('자세히보기')
		}
	})
});
