$(document).ready(function(){
	$('.quick').removeClass('is-active');

	$(window).on("load resize", function () {
		var winH = $(window).height();
		$('.visual').height(winH);
		headerH = $('.header').height();
		$(".etc__box-link").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top-headerH+5}, 500);
	    });
	});
	$(window).on("load",function(){
		visualAniPlay();
	})
	function visualAniPlay(){
		$('.visual').addClass('is-active');
	}
	function quickHide(){
		if ($('body').attr('data-mobile') == 'false'){
			var winH = $(window).height();
			if ($(window).scrollTop()<winH) {
				$('.quick').stop().removeClass('is-active');
			}
			else {
				$('.quick').stop().addClass('is-active');
			}
		}
	}
	quickHide();
	$(window).scroll(function() {
		quickHide();
	});
	$('.price__btn').on('click',function(e){
		e.preventDefault();
		$(this).closest('.price__item').find('.tooltip').fadeIn(300);
	});
	$('.js-page').on('click',function(e){
		e.preventDefault();
		$(this).closest('.price__item').find('.tooltip').fadeOut(300);
	});
	/*시술리스트*/
	$('.surgery__list').slick({
		prevArrow: '.js-surgery-prev',
		nextArrow: '.js-surgery-next',
		slidesToShow:3,
		slidesToScroll: 3,
		infinite: true,
		dots:true,
		adaptiveHeight: true,
		dotsClass:'slider__list',
		responsive: [
		    {
		        breakpoint: 1025,
		        arrows:false,
		        settings: {
		            slidesToShow: 2,
		            slidesToScroll: 2
		        }
		    },
		    {
		        breakpoint: 681,
		        settings: {
		            slidesToShow: 1,
		            slidesToScroll: 1
		        }
		    }
		]
	});

	// banobagi tv
	$(".section-tv__item").eq(0).addClass("is-active"); // default
	$(".section-tv__link").on("click", function(e){
		e.preventDefault();

		var banotvItem = $(this).parents(".section-tv__item");
		var videoId = $(this).attr("data-ytb-id"); // videoId

		banotvItem.addClass("is-active").siblings().removeClass("is-active");

		// change youtube videoId
		banoPlayer.stopVideo();
		banoPlayer.loadVideoById(videoId);

		moveBanoTv();
	});
});

/*
	banoTV Youtube API 로드
*/
var banoTag = document.createElement('script');
banoTag.src = "https://www.youtube.com/iframe_api";
var banoFirstScriptTag = document.getElementsByTagName('script')[0];
banoFirstScriptTag.parentNode.insertBefore(banoTag, banoFirstScriptTag);

var banoPlayer;
var banoVideoId = $(".section-tv__item").eq(0).find(".section-tv__link").attr("data-ytb-id");

function onYouTubeIframeAPIReady() {
	banoPlayer = new YT.Player('section-tv-iframe', {
		height: '360',
		width: '640',
		videoId: banoVideoId
	});
}

function moveBanoTv(){
	// focus
	var banoTop = $(".section-tv").offset().top;
	var headerH = $(".header").height();
	var sectionM = parseInt($(".section-box").css("margin-top"));

	var moveTop = banoTop - headerH - sectionM;

	$('html, body').stop().animate({
		scrollTop: moveTop
	}, 500);
}
